import React from "react";
import "./App.css";
import { NavLink, BrowserRouter, Switch, Route } from "react-router-dom";
import Add from "./containers/Add/Add";
import PostsDetails from "./containers/Add/PostsDetails";
import Home from "./containers/Home/Home";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";

const App = () => (
      <BrowserRouter>
        <div>
          <header className={'header'}>
            <ul>
            <li><NavLink to="/">Home |</NavLink></li>
            <li><NavLink to="/posts/add">Add |</NavLink></li>
            <li><NavLink to="/about">About |</NavLink></li>
            <li><NavLink to="/contacts">Contacts</NavLink></li> 
            </ul>
          </header>

          <div className={'body'}>
          <Switch>
            <Route exact path="/"> <Home /></Route>
            <Route exact path="/posts/add" component={Add} />
            <Route exact path="/about" component={About} />
            <Route exact path="/contacts" component={Contacts} />
            <Route exact path="/posts/:id" component={PostsDetails} />
          </Switch>
          </div> 
        </div>
      </BrowserRouter>
  )


export default App;
