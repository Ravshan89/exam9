import axios from "axios";

const instance = axios.create({
  baseURL: "https://exam9-1726e.firebaseio.com/"
});

export default instance;