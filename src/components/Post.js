import React, { Component } from "react";
import "./Post.css";
import { Link } from 'react-router-dom';


class Post extends Component {
  
  render() {
    return (
      <div className="Post justify-content-center align-items-center">
        <div>created at: {this.props.creationDate}</div>
        <div>title: {this.props.title}</div>
        <div>
            <button className="OrderButton"  onClick={()=> this.props.click}> 
                <Link to={`/posts/${this.props.id}`}> Read More</Link>
            </button>
        </div>      
      </div>
    );
  }
}

export default Post;
