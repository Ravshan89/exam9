import React from 'react';
import './Post.css'

 const PostDelete = (props) => {

    return (
        <div className="postform">
            <input type ="button" value="Delete" onClick={props.delete}/>
        </div>
    );
}
export default PostDelete;