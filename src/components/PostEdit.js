import React from 'react';
import './Post.css'

 const PostEdit = (props) => {

    return (
        <div className="postform">
            <input type="text" placeholder="Edit post" value={props.description} onChange={props.newform}/>
            <input type ="button" value="Edit" onClick={props.edit}/>
        </div>
    );
}
export default PostEdit;