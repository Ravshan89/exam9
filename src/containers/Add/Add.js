import React, { Component } from "react";
import "./Add.css";
import axios from "../../axios-post";


class Add extends Component {
  state = {
    postTitleValue: "",
    postDescriptionValue: "",
    currentId: 0,
    posts: []
  };

  componentDidMount() {
    axios
      .get(".json")
      .then(response => {
          if(response.data){
            const getPosts = response.data.posts;
            const getId = response.data.currentId;
            this.setState({ posts: getPosts });
            this.setState({ currentId: getId });
          }        
      }).catch(error => {
        console.log(error);
      });
  }

  inputPostDescription = event => {
    const inputValue = event.target.value;
    this.setState({ postDescriptionValue: inputValue });
  };

  inputPostTitle = event => {
    const inputValue = event.target.value;
    this.setState({ postTitleValue: inputValue });
  };

  addNewPost = () => {
    const descriptionValue = this.state.postDescriptionValue;
    const titleValue = this.state.postTitleValue;    

    let postId = +this.state.currentId;
    const date = this.getDateToString();

    const newPost = {
      creationDate: date,
      title: titleValue,
      description: descriptionValue,
      id: postId
    };
    const allPosts = this.state.posts;
    allPosts.push(newPost);


    const json = {
        currentId: postId,
        posts: allPosts
    }
    console.log('postId ' + postId)
    axios.put(".json", json);
  };

  getDateToString() {
    const date = new Date();
    var stringDate =
      date.getDate() +
      "." +
      date.getMonth() +
      "." +
      date.getFullYear() +
      " " +
      date.getHours() +
      ":" +
      date.getMinutes();
    return stringDate;
  }
    
  removePost = (id) => {      
    axios.delete('/posts/' + id + '.json').then(response => {
      const index = this.state.posts.findIndex(t => t.id === id);
      const posts = [...this.state.posts];
      posts.splice(index, 1);
      this.setState({ posts });
    });
  }

  editPost= (id) => {      
    const editedPost = {
      name: this.state.currentPost
    }
    axios.put('/posts/' + id + '.json', editedPost).then(response => {
      const index = this.state.posts.findIndex(t => t.id === id);
      const posts = [...this.state.posts];
      posts[index].name = editedPost.name;
      this.setState({ posts });
    });
  }

  render() {
    return (
      <div className="AddNewPost">
          <h1>My Blog</h1>
          <h1>Add new post</h1>
        <div>
          <b>Title</b>
          <input className="title" type="text" onChange={this.inputPostTitle} />
        </div>
        <div>
          <b>Description</b>
          <textarea className="desc" type="text"
            onChange={this.inputPostDescription}
          />
        </div>

        <button name="but" onClick={this.addNewPost}>Save</button>
      </div>
    );
  }
}

export default Add;