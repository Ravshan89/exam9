import React, { Component } from "react";
import axios from "../../axios-post";

class PostsDetails extends Component {
  state = {
    post: {}
  };

  componentDidMount() {
    let id = this.props.match.params.id
        console.log(id)
        axios.get('/posts/${id}.json').then((response) => {
          console.log({response});
            this.setState({ post : response.data });
        }).then(()=>{
            console.log(this.state.post);
        }).catch(error => {
            console.log(error);
        });
  }


  render() {
    return (
      <div className="container">
        <div className="Post justify-content-center align-items-center">
          <div>Created on: {this.state.post.creationDate}</div>
          <div>Title: {this.state.post.title}</div>
          <div>Description: {this.state.post.description}</div>
        </div>
      </div>
    );
  }
}

export default PostsDetails;
