import React, { Component } from "react";
import axios from "../../axios-post";
import Post from "../../components/Post";
import '../../components/Post.css';


class Home extends Component {
    state = {
      posts: []
    };
  
    componentDidMount() {
      axios
        .get(".json")
        .then(response => {
          const getPosts = response.data.posts;
          console.log(getPosts);
          this.setState({ posts: getPosts });
        })
        .catch(error => {
          console.log(error);
        });
    }

    readMoreHandler = () => {
      const queryParams = [];
      queryParams.push('id=' + this.props.id);
      queryParams.push('creationDate=' + this.props.creationDate); 
      queryParams.push('title=' + this.props.title);
      queryParams.push('description=' + this.props.description);   
      
      const queryString = queryParams.join('&');
      console.log(queryString);
      this.props.history.push({
          pathname: '/posts/:id',
          search: '?' + queryString
      });
  }

  
    render() {
      return (
        <div className="container">
            <h1>My Blog</h1>
          {this.state.posts.map(post => {
            return (
              <Post
                key={post.id}
                id={post.id}
                creationDate={post.creationDate}
                title={post.title}
                description={post.description}
                click={() => this.readMoreHandler(this.props.id)}
                remove={() => this.removePost(post.id)}
                edit={() => this.editPost(post.id)}
              />
            );
          })}
        </div>
      );
    }
  }
  
  export default Home;
  